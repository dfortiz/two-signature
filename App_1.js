import React from 'react';
import Expo from 'expo';
import ExpoPixi from 'expo-pixi';

import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component {

  handleClick() {
    alert('ready ked!');
  }

  render() {
    return (
      <Expo.GLView
        style={{ flex: 1 }}
        onContextCreate={async context => {
          const app = ExpoPixi.application({ context });
          const sprite = await ExpoPixi.spriteAsync('http://i.imgur.com/uwrbErh.png');
          app.stage.addChild(sprite);
        }}
        // onReady={this.handleClick}
      />
    );
  }
}

// file:///data/user/0/host.exp.exponent/cache/ExperienceData/%2540dfortiz%252Ftwo-signature/uwrbErh.png]
// /Users/frankortiz/devs/reactnative/two-signature
//             'file:///Users/frankortiz/devs/reactnative/two-signature',

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

/*

[23:37:04] [Unhandled promise rejection: Error: unexpected url: file:///data/user/0/host.exp.exponent/cache/ExperienceData/%2540dfortiz%252Ftwo-signature/uwrbErh.png]
- node_modules/react-native/Libraries/BatchedBridge/NativeModules.js:146:41 in createErrorFromErrorData
- node_modules/react-native/Libraries/BatchedBridge/NativeModules.js:95:55 in <unknown>
- ... 5 more stack frames from framework internals

[23:37:20] The syntax "import Expo from 'expo'" has been deprecated in favor of "import { A, B, C } from 'expo'" or "import * as Expo from 'expo'". This sets us up to support static analysis tools like TypeScript and dead-import elimination better in the future. The deprecated import syntax will be removed in SDK 32.
- node_modules/expo/build/environment/logging.js:25:23 in warn
- node_modules/expo/build/Expo.js:691:4 in get
* App.js:15:7 in render
- node_modules/react-native/Libraries/Renderer/oss/ReactNativeRenderer-dev.js:10563:21 in finishClassComponent
- node_modules/react-native/Libraries/Renderer/oss/ReactNativeRenderer-dev.js:14091:21 in performUnitOfWork
- ... 17 more stack frames from framework internals

*/