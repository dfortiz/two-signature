# Project Signature

To get started:

### `Download the Expo App for your device: `
Download Expo for Android at http://bit.ly/2bZq5ew or iOS at http://apple.co/2c6HMtp


### `Test in your own device: `
Follow the link provided by the developer :)

### `Build tips `

expo build:android
expo fetch:android:keystore
expo build:status

### `Expo Website `

https://exp.host/@dfortiz/two-signature


### `Download APK `

https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40dfortiz/two-signature-7ceedb6960d64b97b7bbe71973af253a-signed.apk
