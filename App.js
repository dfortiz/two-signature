//import Expo from 'expo';
//import { takeSnapshotAsync, ImagePicker, Expo } from 'expo';

import * as ExpoPixi from 'expo-pixi';
import React, { Component } from 'react';
import { Image, Platform, AppState, StyleSheet, Button, Text, View, CameraRoll } from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";

const isAndroid = Platform.OS === 'android';
function uuidv4() {
  //https://stackoverflow.com/a/2117523/4047926
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}


class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Welcome dear user ...</Text>
        <Button
          title="Go to Signature"
          onPress={() => this.props.navigation.navigate('MySignature')}
        />
      </View>
    );
  }
}


 class MySignatureScreen extends React.Component {

  constructor(props, context) {

    super(props, context);
      this.state = {
        image: null,
        strokeColor: 0,
        lines: [],
        appState: AppState.currentState,
      };
    }

  handleAppStateChangeAsync = nextAppState => {

    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {

      if (isAndroid && this.sketch) {
        this.setState({ appState: nextAppState, id: uuidv4(), lines: [] });
        return;
      }
    }
    this.setState({ appState: nextAppState });
  };

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChangeAsync);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChangeAsync);
  }

  async componentWillMount() {
  }


  _onChangeAsync = async () => {
      console.log('this.....state>.' , this.state);
      const { uri } = await this.sketch.takeSnapshotAsync();

      this.setState({
        image: { uri }
      });

    };

  _onReady = () => {
    this.sketch.clear();

    setTimeout(() => {
      this.sketch.renderer._update();
      console.log('setTimeout ready ');
    }, 10);

  }



  _save = async () => {
    console.log('_save');

    if (this.state.appState.match(/inactive|background/)) {
      this.setState({ appState: "active", id: uuidv4(), lines: [] });
    }
    else {
      this.setState({ appState: "background", id: uuidv4(), lines: [] });
    }
    console.log('this.....SAVE>: ' , this.state);
    // const signature_result = await this.sketch.takeSnapshotAsync({
    //   format: 'png',
    //   //result: 'file',
    //   result: 'base64',
    //   quality: 1,
    //   height: 100,
    //   width: 100,
    // });
    // console.log(signature_result);
    // console.log(`Snapshot saved ${JSON.stringify(signature_result)}\n`);

  }

  render() {
    const color = 0x000000;
    const width = 5;
    const alpha = 0.5;

    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <View style={styles.sketchContainer}>
            <ExpoPixi.Signature
              ref={ref => (this.sketch = ref)}
              style={styles.sketch}
              strokeColor={color}
  						strokeWidth={width}
  						strokeAlpha={alpha}
              onChange={this._onChangeAsync}
              onReady={this._onReady}
            />
          </View>
        </View>
        <Button
          color={'green'}
          title="accept"
          style={styles.button}
          onPress={ this._save }
        />
        <Button
          color={'red'}
          title="undo"
          style={styles.button}
          onPress={() => {
            this.sketch.undo();
          }}
        />
      </View>
    );
  }
} // class MySignatureScreen




const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  sketch: {
    flex: 1,
    backgroundColor: 'yellow'
  },
  sketchContainer: {
    height: '100%',
    backgroundColor: 0xffffff,
  },
  image: {
    flex: 1,
  },
  label: {
    width: '100%',
    padding: 5,
    alignItems: 'center',
  },
  button: {
    zIndex: 1,
    padding: 12,
    minWidth: 56,
    minHeight: 48,
  },
});

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    MySignature: MySignatureScreen,
  },
  {
    initialRouteName: 'Home',
  }
);


const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
